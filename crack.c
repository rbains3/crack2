#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *guessHash = md5(guess, strlen(guess) - 1);
    strcat(guessHash, "\n");

    // Compare the two hashes
    if(strcmp(hash, guessHash) == 0)
    {
        free(guessHash);
        return 1;
    }
    else
    {
        //printf("%s %s\n", hash, guessHash);
        free(guessHash);
    return 0;
    }

    // Free any malloc'd memory
    free(guessHash);
}

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **readfile(char *filename)
{
    FILE *f;
    f = fopen(filename,"r");
    if(!f)
    {
        printf("Could not open file!");
        exit(1);
    }
    char **m = malloc(200*sizeof(char*));//using malloc to allocate memory
    
    int totalSize=0;
    int i = 0;
    char temp[100];
    
    while(fgets(temp,100, f) != NULL)
    {
        if(i == totalSize)
        {
            totalSize += 100;
            
            char **newm = (char**)realloc(m, totalSize * sizeof(char *));
            
            if(newm == NULL)
            {
                exit(1);
            }
            else if(newm != m)
            {
                m = newm;
            }
        }
        
        int len = strlen(temp);
        m[i] = (char*)malloc((len + 1) * sizeof(char));
        strcpy(m[i], temp);
        i++;
    }
    
    fclose(f);
    
    if(i == totalSize)
    {
        char **newm = (char**)realloc(m, (1 + totalSize) * sizeof(char *));
        
        if(newm == NULL)
        {
        exit(1);
        }
        else if(newm != m)
        {
            m = newm;
        }
    }
    
    m[i + 1] = NULL;
    
    return m;
}
            
        
    


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the hash file into an array of strings
    char **hashes = readfile(argv[1]);

    // Read the dictionary file into an array of strings
    char **dict = readfile(argv[2]);
    


    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    int hashCounter = 0;
    int dictCounter = 0; 
    
    while(hashes[hashCounter] != NULL)
    {
        while(dict[dictCounter] != NULL)
        {
            if(tryguess(hashes[hashCounter], dict[dictCounter]) == 1)
            {
                printf("%s %s", hashes[hashCounter], dict[dictCounter]);
            }
           //printf("%d : %d -> %s:%s\n", dictCounter, hashCounter, hashes[hashCounter], dict[dictCounter]);
            dictCounter++;
        }
        
        dictCounter = 0;
        hashCounter++;
    }
    /*
    int i = 0;
    while(hashes[i] != NULL)
    {
        printf("%s", hashes[i]);
        i++;
    } */
}
    


